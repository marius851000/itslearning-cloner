from itsbase import itsclass
from itsfolder import folder

class course(itsclass):
    def get_tag(self, tag):
        if tag == "ressource":
            self.parse_ressource()

    def parse_ressource(self):
        ressourceURL = self.root.get_page_tree("ContentArea/ContentArea.aspx?LocationID="+self.get("id")+"&LocationType=1")
        id = ressourceURL.xpath("//a[@id='link-resources']")[0].attrib["href"].split("=")[1]
        self.set("ressource",folder(self.root, {"id":id,"path":"/"}))
