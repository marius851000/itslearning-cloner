from lxml import html as etreeHTML
import os
import json

class itsMail:
    mailboxMailPage = "Messages/view_message.aspx?MessageFolderId=1&messageId={}"
    def __init__(self, master, mailId):
        self.master = master
        self.mailId = mailId
        self._dump = self.dump()

    def dump(self):
        #TODO: cleanup
        pageURL = self.mailboxMailPage.format(self.mailId)
        page = self.master.get_page_tree(pageURL)
        result = {}
        result["author"] = page.xpath("//table[@class='h-width-100 h-mrb5 description']/tr/td/a/span/text()")[0]
        result["date"] = page.xpath("//table[@class='h-width-100 h-mrb5 description']/tr/td/text()")[0]
        result["receiver"] = page.xpath("//table[@class='h-width-100 h-mrb5 description']/tr/td/text()")[1:]
        result["pieces"] = {}
        for piece in page.xpath("//table[@class='h-width-100 h-mrb5 description']/tr/td/a[@target='_blank']"):
            name = piece.text
            download = piece.attrib["href"]
            result["pieces"][name] = {"name": name, "url": download}
        result["content"] = etreeHTML.tostring(page.xpath("//div[@class='h-userinput']")[0])
        return(result)


class ItsMailBox:
    mailboxMainPage = "Messages/Messages.aspx"
    mailboxListPage = "Messages/InternalMessages.aspx?MessageFolderId={}"
    def __init__(self, master):
        self.master = master

    def mirror(self, root="./"):
        # format
        #boxId - boxName
        #  \-- mailId - mailName
        #       |-- mail.json #meta
        #       |-- content.html
        #       \-- piece-nomdelapiècejointe #include extension
        os.makedirs(root, exist_ok=True)
        boxes = self.list_folder()
        for boxId in boxes:
            box = boxes[boxId]
            boxSaveDir = os.path.join(root, boxId + " - " + box["name"])
            os.makedirs(boxSaveDir, exist_ok=True)
            mails = self.list_mail(boxId)
            for mailId in mails:
                mailDict = mails[mailId]
                mail = self.get_mail(mailId)
                mailSaveDir = os.path.join(boxSaveDir, mailId + " - " + mailDict["title"])
                os.makedirs(mailSaveDir, exist_ok=True)
                content = open(os.path.join(mailSaveDir, "content.html"),"wb")
                content.write(mail._dump["content"])
                content.close()
                mailFile = mail._dump
                del mailFile["content"]
                mailFile["id"] = mailId
                content = open(os.path.join(mailSaveDir, "mail.json"), "w")
                content.write(json.dumps(mailFile))
                content.close()
                pieces = mail._dump["pieces"]
                for pieceId in pieces:
                    piece = pieces[pieceId]
                    pieceSaveDir = os.path.join(mailSaveDir, "piece - "+piece["name"])
                    self.master.download_content(self.master.plages["itslearning"] +piece["url"], pieceSaveDir)


    def list_folder(self):
        page = self.master.get_page_tree(self.mailboxMainPage)
        pageHTML = None
        for script in page.xpath("//script"):
            if script.text is not None:
                if ',"html_data" : {"data" : "' in script.text:
                    pageHTML = etreeHTML.fromstring(script.text.split(',"html_data" : {"data" : "')[1].split("\"")[0])
        result = {}
        for li in pageHTML.xpath("./li")[-1].xpath("./ul/li"):
            thisFolder = {
                "id": li.attrib["id"][1:],
                "icon": li.xpath("./a/img")[0].attrib["src"],
                "name": str(etreeHTML.tostring(li.xpath("./a/img")[0]))[2:-1].split(">")[1].split(" (")[0] #HACK: so ugly
            }
            result[thisFolder["id"]] = thisFolder
        return result

    def list_mail(self, boxid):
        #TODO: mail over multiple page
        pageUrl = self.mailboxListPage.format(boxid)
        page = self.master.get_page_tree(pageUrl)
        table = page.xpath("//table")[0]
        result = {}
        for tr in table:
            if tr.attrib["id"] != "_table_0":
                #empty box
                if tr.attrib["id"] == "_table_1" and len(tr) == 1:
                    break
                td = tr.findall("td")
                titleSpan = td[3].xpath("./a/span/text()")
                thisMail = {
                    "authorName": td[2].xpath("./div/text()")[0],
                    "title": titleSpan[0],
                    "resume": titleSpan[1],
                    "date": td[-1].xpath("./div/text()")[0],
                    "id": td[2].attrib["onclick"].split("'")[1]
                }
                result[thisMail["id"]] = thisMail
        return result

    def get_mail(self, mailId):
        return itsMail(self.master, mailId)
