class itsclass:
    def __init__(self, root, initial_data = {}):
        self.root = root
        self._data = initial_data

    def set(self, tag, value):
        self._data[tag] = value

    def get(self, tag):
        if tag in self._data:
            return self._data[tag]
        self.get_tag(tag)
        if tag in self._data:
            return self._data[tag]
        raise BaseException("impossible to get the tag '%s'."%tag)

    def get_tag(self, tag):
        print("Warning: get_tag is not changed for this class. Will likely lead to an error.")
