import requests
from lxml import html as etreeHTML
from itscourse import course as courseClass
from itsmailbox import ItsMailBox

class itslearningconnection:
    def __init__(self, baseURL):
        # TODO: is_valid_url(self.baseURL)
        self.cookies = requests.cookies.RequestsCookieJar()
        self.plages = {"itslearning":baseURL}

    def connect_sessionid(self, sessionid):
        self.cookies.set("ASP.NET_SessionId",sessionid)

    def get_page(self, page, full = False, plage = "itslearning", data = {}):
        if full:
            pageURL = page
        else:
            pageURL = self.plages[plage] + page
        print("getting %s"%pageURL)
        useragent = "Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"
        r = requests.get(pageURL, cookies=self.cookies, data = data, headers = {"User-Agent":useragent})
        a = open("lastpage.html","wb")
        a.write(r.content)
        a.close()
        self.cookies.update(r.cookies)
        return r.content#.decode("utf-8")

    def download_content(self, url, dest):
        print("downloading the file at "+url)
        r = requests.get(url, cookies = self.cookies)
        f = open(dest,"wb")
        f.write(r.content)
        f.close()

    def get_page_tree(self, page, full = False, data = {}):
        html = self.get_page(page,full,data=data)
        return etreeHTML.fromstring(html)

    def get_course_table(self):
        page = self.get_page_tree("Course/AllCourses.aspx")
        table = page.xpath("//div[@class='tablelisting']/table")
        assert(len(table)==1)
        return table[0]

    def get_course_list(self):
        retour = []
        for course in self.get_course_table().xpath("./tr"):
            if not (course.attrib.get("id")[-1] == "0" and course.attrib.get("id")[-2] == "_"):
                actual_course = courseClass(self, {
                    "name":course.xpath("./td/a[@class='ccl-iconlink']/span/text()")[0],
                    "id":course.xpath("./td[@class=' checkboxcolumn']/input[@type='checkbox']")[0].attrib["value"],
                    "lastupdate":course.xpath("./td/text()")[0], #TODO: transform this to parsable date
                    "lastview":course.xpath("./td/text()")[1], #TODO: idem
                    "state":course.xpath("./td/text()")[2],
                    "role":course.xpath("./td/text()")[3]
                })
                retour.append(actual_course)
        return retour

    def get_extern_service(self):
        page = self.connect_extern_service()
        retours = {}
        elements = page.xpath("//li[@onclick]")
        for element in elements:
            category = element.xpath("./div/a/span/text()")[0]
            url = element.attrib.get("onclick").split("\"")[1]
            classe = None
            if category == "Pronote":
                from pronote.pronote import pronote
                classe = pronote(self)
                classe.connect_itsurl(url)
            if classe == None:
                print("Warning: no connecter for the "+category+" service.")
            else:
                retours[category] = classe
        return retours

    def connect_extern_service(self):
        connect = self.get_page_tree("ExtensionModule/ExtensionModuleIntermediatePage.aspx?ExtensionModuleSetupId=4&ItslearningSection=TopMenu")
        scripts = connect.xpath("//script[@type='text/javascript']/text()")
        desturl = scripts[0].split("window.location=\"")[1].split("\"")[0]
        second_page = self.get_page_tree(desturl,full=True)
        script_second = second_page.xpath("//script[@type='text/javascript']/text()")[0]
        final_url = "https://servicesexternes.itslfr-aws.com/"+script_second.split('$("#liste_interop").load("')[1].split("\"")[0]
        final_page = self.get_page_tree(final_url,full=True,data = {
            "action":"load_iop",
            "data":"role=Learner&school=0850027T", #IMPORTANT: automatically get the school ID
            "detail":"0"
        })
        return(final_page)

    @property
    def mailbox(self):
        return ItsMailBox(self)
