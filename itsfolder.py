from itsbase import itsclass
from lxml import html as etree
import os

class file(itsclass):
    def get_tag(self, tag):
        if tag == "download_url":
            self.parse_page()

    def parse_page(self):
        page = self.root.get_page_tree("LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId="+self.get("id"))
        contentpage = self.root.get_page_tree(page.xpath("//iframe")[0].attrib["src"], full = True)
        download_url = contentpage.xpath("//a[@download]")[0].attrib["href"]
        self.set("download_url",download_url)

class folder(file):
    def get_tag(self, tag):
        if tag == "elems":
            self.parse_page()

    def parse_page(self):
        page = self.root.get_page_tree("Folder/processfolder.aspx?FolderID="+self.get("id"))
        folder_elems = page.xpath("//tbody[@id='ctl00_ContentPlaceHolder_ProcessFolderGrid_TB']/tr") #TODO: test for multiple page folder
        if folder_elems[0].xpath("td")[0].attrib["class"] == "emptytablecell":
            self.set("elems",[])
            return
        sub_elems = {}#shorted by name
        for elem in folder_elems:
            isfolder = (elem.xpath("./td/img")[0].attrib["src"].split("/")[-1] == "element_folder16.png")
            temp_data = elem.xpath("./td")[1]
            id = temp_data.xpath("a")[0].attrib["href"].split("=")[1]
            name = temp_data.xpath("a")[0].attrib["title"]
            publishing_information = elem.xpath("./td")[2] #TODO: analyse
            # TODO: categorie Actif
            data = {
                "id":id,
                "name":name
            }
            if isfolder:
                sub_elems[name] = folder(self.root, data)
            else:
                sub_elems[name] = file(self.root, data)
        self.set("elems",sub_elems)

    def clone(self, destfolder):
        # first, create the folder if it is not already created
        os.makedirs(destfolder, exist_ok=True)
        # second, we list sub element
        elems = self.get("elems")
        # on télégearge chacun de ces éléments
        for elem in elems:
            elem_elem = self.get("elems")[elem]
            if isinstance(elem_elem, folder):
                elem_elem.clone(destfolder+elem_elem.get("name")+"/")
            else:
                destination = destfolder+elem_elem.get("name")
                if os.path.isfile(destination+".finished"):
                    self.root.download_content(elem_elem.get("download_url"),destination)
                    temp_file = open(destination+".finished","w")
                    temp_file.close()
