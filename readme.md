itslearning archiver

you can archive your itslearning mail and course (do not mirror pronote, nor anything else).

To use it, you need your tempory session id.

get the session id:
## firefox
1. You should connect to elyco with firefox
2. On firefox, install the extension "cookie manager". https://addons.mozilla.org/fr/firefox/addon/a-cookie-manager
3. once installed, go to the itslearning page.
4. click on the cookie manager icon
5. click on "open cookie manager for the current tab"
6. In the table, find "ASP.NET_SessionId". Copy the code at the right of it (should be similar to ligenfeseasif6vjhgbduuv4)

## after this
1. Once you have you session id, open the file "main.py"
2. Copy the session id to replace "yoursessionid". The line should be similar to
		a.connect_sessionid(sessionid = "ligenfeseasif6vjhgbduuv4")
3. execute the file main.py. If it made error, that is probably because you don't have the good python extension. install it and rerun it. It should create a c and mail folder, and take between 30s and 2min to run.
